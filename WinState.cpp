#include "WinState.h"
#include "Music.h"

WinState::WinState(GameStateManager::StateOptions options)
{
	e = Event::getInstance();
	s = Surface::getInstance();
	_has_ended = false;
	bg = s->load_optimized_image("assets/win.png");
	Music::getInstance()->set_music(Music::NONE);
}


WinState::~WinState(void)
{
	SDL_FreeSurface(bg);
}

void WinState::handle_events()
{
	if (e->keys[SDLK_ESCAPE])
	{
		_has_ended = true;
	}
}

void WinState::logic()
{
	return;
}

void WinState::render()
{
	s->draw(0, 0, bg);
}

bool WinState::has_ended()
{
	return _has_ended;
}