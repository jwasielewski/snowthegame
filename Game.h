#include "SDL.h"
#include "Surface.h"
#include "Event.h"
#include "Music.h"
#pragma once
class Game
{
public:
	Game(void);
	~Game(void);

	bool init();
	void run();

private:
	Surface* main_window;
	Event* e;
	Music* m;
	const int FRAMES_PER_SECOND;
};

