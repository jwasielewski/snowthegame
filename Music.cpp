#include "Music.h"


Music::Music(void)
{
	battle = Mix_LoadMUS("music/pokemon_xy_battle_8bit.ogg");
	background = Mix_LoadMUS("music/game_of_thrones_8bit.ogg");
	active_music = NULL;
}


Music::~Music(void)
{
	Mix_HaltMusic();
	Mix_FreeMusic(battle);
	Mix_FreeMusic(background);
	Mix_CloseAudio();
}

Music* Music::getInstance()
{
	static Music s;
	return &s;
}

bool Music::init()
{
	if(Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 )) return true;
	return false;
}

void Music::play()
{
	if(active_music == NULL) return;
	if( active_music != NULL && Mix_PlayingMusic() == 0 )
    {
		Mix_PlayMusic(active_music, -1);
	}
}

void Music::set_music(MUSIC m)
{
	switch (m)
	{
	case Music::BACKGROUND:
		if(active_music != background)
		{
			Mix_HaltMusic();
			active_music = background;
		}
		Mix_ResumeMusic();
		break;
	case Music::BATTLE:
		if(active_music != battle)
		{
			Mix_HaltMusic();
			active_music = battle;
		}
		Mix_ResumeMusic();
		break;
	case Music::NONE:
		Mix_PauseMusic();
		active_music = NULL;
		break;
	default:
		break;
	}
}