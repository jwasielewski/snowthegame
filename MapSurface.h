#pragma once
#include <string>
#include "EnemyManager.h"
#include "Textures.h"
#include "Surface.h"
#include "Player.h"
#include "Event.h"
#include <vector>
class MapSurface
{
public:
	MapSurface(char *, Textures*, Player*, EnemyManager*, int);
	~MapSurface(void);

	void render();
	void handle_events();
	void configure();
	std::string check_state();

private:
	void load_map(char *);
	bool is_there_enemy(int,int);
	int get_enemy_id_at_pos(int,int);
	std::vector<std::vector<char> > map;
	Textures* t;
	Surface* s;
	Event* e;
	Player* p;
	EnemyManager* em;
	std::string state;
	std::string configfile;
	std::vector<int> enemies_id;
	int nl_x, nl_y; // pozycja przej�cia na nast�pny poziom
	int w_x, w_y; // pozycja wygranej w grze
	int h_x, h_y; // pozycja kafelka lecz�cego gracza
	int map_id;
};
