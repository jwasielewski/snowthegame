#include "GameState.h"
#include <map>
#include <string>

struct cmp_str
{
	bool operator()(std::string a, std::string b)
	{
		return a < b;
	}
};

#pragma once
class GameStateManager
{
public:
	static GameStateManager* getInstance();
	~GameStateManager(void);

	typedef std::map<std::string, std::string, cmp_str> StateOptions;

	enum GameStates { STATE_MENU, STATE_LEVEL, STATE_BATTLE, STATE_WIN, STATE_LOSE };

	void request_next_state(GameStates state, StateOptions options = StateOptions("NULL", "NULL"));
	GameState* get_actual_state();
	void check_last_state();

private:
	GameStateManager(void);
	GameState** states;
	int states_counter;
};

