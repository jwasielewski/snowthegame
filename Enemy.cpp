#include "Enemy.h"

Enemy::Enemy(int _id, int _x, int _y, int mi)
{
	life = 100;
	alive = true;
	id = _id;
	map_id = mi;
	x = _x;
	y = _y;

	s = Surface::getInstance();
	surface = s->load_optimized_image("assets/enemy.png");
	frame.x = 0;
	frame.y = 0;
	frame.w = 32;
	frame.h = 48;
}

Enemy::~Enemy(void)
{
	SDL_FreeSurface(surface);
}

void Enemy::set_id_xy(int _id, int _x, int _y)
{
	id = _id;
	x = _x;
	y = _y;
}

bool Enemy::check_map_id(int x)
{
	return ( map_id == x );
}

int Enemy::ID()
{
	return id;
}

bool Enemy::is_alive()
{
	return alive;
}

int Enemy::get_life_points()
{
	return (life > 0) ? life : 0;
}

void Enemy::set_damage(int dmg)
{
	life -= dmg;
	if(life <= 0)
	{
		alive = false;
	}
}

void Enemy::draw()
{
	s->draw(x*32,y*32-48,surface,&frame);
}

bool Enemy::position(int _x, int _y)
{
	return ( (x == _x) && (y == _y) );
}

#if DEBUG == 1
std::string Enemy::gp()
{
	return std::to_string(x) + "," + std::to_string(y);
}
#endif