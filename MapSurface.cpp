#include "GameStateManager.h"
#include "MapSurface.h"
#include "Surface.h"
#include "Textures.h"
#include "Event.h"
#include "Player.h"
#include "SDL.h"
#include <fstream>
#include <string>
#include <sstream>

#include "Debug.h"

#if DEBUG == 1
#include <iostream>
using namespace std;
#endif

MapSurface::MapSurface(char *p, Textures* _t, Player* _p, EnemyManager* _e, int id)
{
	this->p = _p;
	load_map(p);
	t = _t;
	s = Surface::getInstance();
	e = Event::getInstance();
	em = _e;
	map_id = id;
	enemies_id = em->get_enemies_id(id);
	state = "";
	std::string s(p); s += ".config";
	configfile = s;
	nl_x = nl_y = w_x = w_y = -1;
}


MapSurface::~MapSurface(void)
{
}

void MapSurface::load_map(char *path)
{
	std::ifstream file(path);
	std::string line;
	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			std::istringstream is(line);
			std::vector<char> row;
			char k;
			while((is >> k))
			{
				row.push_back(k);
			}
			map.push_back(row);
		}
	}
	file.close();
}

void MapSurface::configure()
{
	std::ifstream file(configfile);
	std::string line;
	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			std::istringstream is(line);
			char k; int lx, ly;
			while((is >> k >> lx >> ly))
			{
				if(k == 'P') { p->set_position(lx, ly); }
				else if(k == 'N') { nl_x = lx; nl_y = ly; map[ly][lx] = 'N'; }
				else if(k == 'W') { w_x = lx; w_y = ly; map[ly][lx] = 'W'; }
				else if(k == 'H') { h_x = lx; h_y = ly; map[ly][lx] = 'H'; }
			}
		}
	}
	file.close();
}

void MapSurface::render()
{
	SDL_Rect r;
	for(int j=0; j<19; ++j)
	{
		for(int i=0; i<25; ++i)
		{
			r = t->r(map[j][i]);
			s->draw(i*32, j*32, t->Tiles, &r);
		}
	}
	for(auto &x : enemies_id)
	{
		if(em->at(x)->is_alive())
		{
			em->at(x)->draw();
		}
	}
	p->render();
}

void MapSurface::handle_events()
{
	if(p->is_animation_running()) return;

	int x = p->gx();
	int y = p->gy();

	if(x == nl_x && y == nl_y)
	{
		state = "NEXT_LEVEL";
		return;
	}
	if(x == w_x && y == w_y)
	{
		state = "WIN";
		return;
	}
	if(p->get_life_points() <= 0)
	{
		state = "LOSE";
		return;
	}
	if(x == h_x && y == h_y)
	{
		(p->get_life_points() < 100) ? p->set_damage(-(100 - p->get_life_points())) : 0;
	}
	if(map[y][x] == 'L')
	{
		p->set_damage(666);
		return;
	}

	if (e->keys[SDLK_w] || e->keys[SDLK_UP])
	{
		if(y-1 >= 0 && t->cg(map[y-1][x]) && !is_there_enemy(x,y-1))
		{
			p->up();
		}
		p->set_direction(Player::Direction::UP);
	}
	else if (e->keys[SDLK_s] || e->keys[SDLK_DOWN])
	{
		if(y+1 < 19 && t->cg(map[y+1][x]) && !is_there_enemy(x,y+1))
		{
			p->down();
		}
		p->set_direction(Player::Direction::DOWN);
	}	
	else if (e->keys[SDLK_a] || e->keys[SDLK_LEFT])
	{
		if(x-1 >= 0 && t->cg(map[y][x-1]) && !is_there_enemy(x-1,y))
		{
			p->left();
		}
		p->set_direction(Player::Direction::LEFT);
	}	
	else if (e->keys[SDLK_d] || e->keys[SDLK_RIGHT])
	{
		if(x+1 < 25 && t->cg(map[y][x+1]) && !is_there_enemy(x+1,y))
		{
			p->right();
		}
		p->set_direction(Player::Direction::RIGHT);
	}
	else if (e->keys[SDLK_SPACE])
	{
		int id;
		GameStateManager::StateOptions options;
		switch (p->get_direction())
		{
		case Player::Direction::UP:
			if((id = get_enemy_id_at_pos(x, y-1)) != -1)
			{
				options["ENEMY_ID"] = std::to_string(id);
				GameStateManager::getInstance()->request_next_state(
					GameStateManager::GameStates::STATE_BATTLE,
					options
				);
			}
			break;
		case Player::Direction::DOWN:
			if((id = get_enemy_id_at_pos(x, y+1)) != -1)
			{
				options["ENEMY_ID"] = std::to_string(id);
				GameStateManager::getInstance()->request_next_state(
					GameStateManager::GameStates::STATE_BATTLE,
					options
				);
			}
			break;
		case Player::Direction::LEFT:
			if((id = get_enemy_id_at_pos(x-1, y)) != -1)
			{
				options["ENEMY_ID"] = std::to_string(id);
				GameStateManager::getInstance()->request_next_state(
					GameStateManager::GameStates::STATE_BATTLE,
					options
				);
			}
			break;
		case Player::Direction::RIGHT:
			if((id = get_enemy_id_at_pos(x+1, y)) != -1)
			{
				options["ENEMY_ID"] = std::to_string(id);
				GameStateManager::getInstance()->request_next_state(
					GameStateManager::GameStates::STATE_BATTLE,
					options
				);
			}
			break;
		default:
			break;
		}
	}
#if DEBUG == 1
	else if (e->keys[SDLK_p])
	{
		cout << "\n==================================\n";
		cout << "Player(x,y) = " << x << "," << y << '\n';
		cout << "Enemies:\n";
		for(auto &e : enemies_id)
		{
			cout << '\t' << e << "=> " << em->at(e)->gp() << '\n';
		}
		cout << "check => " << is_there_enemy(x,y) << '\n';
		cout << '\n';
	}
#endif
}

bool MapSurface::is_there_enemy(int x, int y)
{
	for(auto &e : enemies_id)
	{
		if(em->at(e)->is_alive() && em->at(e)->position(x,y+1))
		{
			return true;
		}
	}
	return false;
}

int MapSurface::get_enemy_id_at_pos(int x, int y)
{
	for(auto &e : enemies_id)
	{
		if(em->at(e)->is_alive() && em->at(e)->position(x,y+1))
		{
			return e;
		}
	}
	return -1;
}

std::string MapSurface::check_state()
{
	return state;
}
