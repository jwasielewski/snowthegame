#include "Player.h"
#include "Event.h"
#include "Surface.h"

Player::Player() : ANIMATION_SPEED(4)
{
	life = 100;

	x = 6;
	y = 6;

	rect.x=0;
	rect.y=0;
	rect.w=32;
	rect.h=48;
	surface=Surface::getInstance()->load_optimized_image("assets/player.png");
	direction = Direction::DOWN;

	frames[0].x = 0;
	frames[0].y = 0;
	frames[0].w = 32;
	frames[0].h = 48;

	frames[1].x = 0;
	frames[1].y = 48;
	frames[1].w = 32;
	frames[1].h = 48;

	frames[2].x = 0;
	frames[2].y = 96;
	frames[2].w = 32;
	frames[2].h = 48;

	frames[3].x = 0;
	frames[3].y = 144;
	frames[3].w = 32;
	frames[3].h = 48;

	animation.is_running = false;
	animation.frame = 0;
}

Player::~Player(void)
{
	SDL_FreeSurface(surface);
}

Player* Player::getInstance()
{
	static Player p;
	return &p;
}

void Player::up()
{
	animation.is_running = true;
	animation.frame = 0;
	animation.x = x * 32;
	animation.y = y * 32;
	animation.end_x = x * 32;
	animation.end_y = y * 32 - 32;
	direction = Direction::UP;
}

void Player::down()
{
	animation.is_running = true;
	animation.frame = 0;
	animation.x = x * 32;
	animation.y = y * 32;
	animation.end_x = x * 32;
	animation.end_y = y * 32 + 32;
	direction = Direction::DOWN;
}

void Player::left()
{
	animation.is_running = true;
	animation.frame = 0;
	animation.x = x * 32;
	animation.y = y * 32;
	animation.end_x = x * 32 - 32;
	animation.end_y = y * 32;
	direction = Direction::LEFT;
}

void Player::right()
{
	animation.is_running = true;
	animation.frame = 0;
	animation.x = x * 32;
	animation.y = y * 32;
	animation.end_x = x * 32 + 32;
	animation.end_y = y * 32;
	direction = Direction::RIGHT;
}

void Player::render()
{
	if(animation.is_running)
	{
		if(animation.x == animation.end_x && animation.y == animation.end_y)
		{
			x = animation.end_x / 32;
			y = animation.end_y / 32;
			animation.is_running = false;
			Surface::getInstance()->draw(x*32,y*32-48,surface,&frames[direction]);
			return;
		}
		switch (direction)
		{
		case Player::DOWN:
			Surface::getInstance()->draw(x*32,(animation.y+=ANIMATION_SPEED)-48,surface,&get_anim_frame());
			break;
		case Player::LEFT:
			Surface::getInstance()->draw((animation.x-=ANIMATION_SPEED),y*32-48,surface,&get_anim_frame());
			break;
		case Player::RIGHT:
			Surface::getInstance()->draw((animation.x+=ANIMATION_SPEED),y*32-48,surface,&get_anim_frame());
			break;
		case Player::UP:
			Surface::getInstance()->draw(x*32,(animation.y-=ANIMATION_SPEED)-48,surface,&get_anim_frame());
			break;
		default:
			break;
		}
		(animation.frame == 3) ? animation.frame=0 : ++animation.frame;
	}
	else
	{
		Surface::getInstance()->draw(x*32,y*32-48,surface,&frames[direction]);
	}
}

SDL_Rect Player::get_anim_frame()
{
	SDL_Rect rect;
	rect = frames[direction];
	rect.x = rect.x + 32 * animation.frame;
	return rect;
}

void Player::set_position(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Player::set_direction(Direction d)
{
	direction = d;
}

bool Player::is_animation_running()
{
	return animation.is_running;
}