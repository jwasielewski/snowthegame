#pragma once
#include "SDL.h"
class Event
{
public:
	~Event(void);
	static Event* getInstance();
	void handle_events();
	SDL_Event e;
	bool keys[323];
private:
	Event(void);
};
