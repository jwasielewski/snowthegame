#include "Game.h"
#include "GameStateManager.h"
#include "Surface.h"
#include "Timer.h"
#include "Event.h"
#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_mixer.h"
#include <string>

Game::Game(void) : FRAMES_PER_SECOND(20)
{
	e = Event::getInstance();
	m = Music::getInstance();
}

Game::~Game(void)
{
	TTF_Quit();
	SDL_Quit();
}

bool Game::init()
{
	if((SDL_Init(SDL_INIT_EVERYTHING) == -1) || (TTF_Init() == -1) || m->init())
	{
		return false;
	}

	main_window = Surface::getInstance();
	if(!main_window->init())
	{
		return false;
	}

	return true;
}

void Game::run()
{
	int frame = 0;
	bool quit = false;
	Timer fps;

	GameStateManager* gsm_instance = GameStateManager::getInstance();
	gsm_instance->request_next_state(
		GameStateManager::GameStates::STATE_MENU,
		GameStateManager::StateOptions()
	);
	
	while(quit == false)
	{
		fps.start();

		e->handle_events();
		if(e->e.type == SDL_QUIT)
		{
			quit = true;
			continue;
		}

		if(gsm_instance->get_actual_state() == NULL)
		{
			quit = true;
			continue;
		}

		gsm_instance->get_actual_state()->logic();
		gsm_instance->get_actual_state()->handle_events();
		gsm_instance->get_actual_state()->render();
		main_window->refresh();

		++frame;

		if(fps.get_ticks() < 1000 / FRAMES_PER_SECOND)
        {
            SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - fps.get_ticks() );
        }

		gsm_instance->check_last_state();

		m->play();
	}
}
