#include "EnemyManager.h"
#include <fstream>

EnemyManager::EnemyManager(void)
{

}


EnemyManager::~EnemyManager(void)
{
	for(auto &x : v)
	{
		delete x;
	}
}

EnemyManager* EnemyManager::getInstance()
{
	static EnemyManager e;
	return &e;
}

void EnemyManager::configure()
{
	const char* path = "config/enemies.stgc";
	std::ifstream file;
	file.open(path);
	if(!file.is_open())
	{
		file.close();
		return;
	}

	int id, x ,y, mi;
	while((file >> id >> x >> y >> mi))
	{
		v.push_back(new Enemy(id, x, y, mi));
	}

	file.close();
}

std::vector<int> EnemyManager::get_enemies_id(int i)
{
	std::vector<int> v;
	for(auto &x : this->v)
	{
		if(x->check_map_id(i))
		{
			v.push_back(x->ID());
		}
	}
	return v;
}

Enemy* EnemyManager::at(int i)
{
	for(auto &x : v)
	{
		if(x->ID() == i)
		{
			return x;
		}
	}
	return NULL;
}

int EnemyManager::get_life_at()
{
	return 666;
}
