#pragma once
#include "GameState.h"
#include "GameStateManager.h"
#include "Player.h"
#include "EnemyManager.h"
#include "Surface.h"
#include "Event.h"
#include "SDL_ttf.h"
#include "Character.h"
#include "Music.h"
#include "Clock.h"
class BattleState : public GameState
{
public:
	BattleState(GameStateManager::StateOptions);
	~BattleState(void);

	void handle_events();
	void logic();
	void render();
	bool has_ended();

private:
	bool _has_ended;

	Player* p;
	EnemyManager* em;
	Surface* s;
	Event* e;
	Music* m;
	Clock* clock;

	int enemy_id;
	static bool is_player_turn;

	const int PLAYER_FATALITY_ATTACK;
	const int PLAYER_ATTACK_STRONG;
	const int PLAYER_ATTACK_WEAK;
	const int ENEMY_ATTACK;

	SDL_Color c;
	SDL_Color pturn;
	SDL_Color eturn;

	SDL_Surface* player_image;
	SDL_Surface* enemy_image;
	SDL_Surface* bg;

	SDL_Surface* player_health;
	SDL_Surface* enemy_health;
	SDL_Surface* strong_attack;
	SDL_Surface* weak_attack;
	SDL_Surface* player_turn;
	SDL_Surface* enemy_turn;

	TTF_Font* font;

	class Animation
	{
	public:
		Animation(Surface* screen) { s = screen; is_running = false; }

		void set_params(int x, int ex, int as, SDL_Surface* s, Character* c, int d)
		{
			start_x = actual_x = x;
			end_x = ex;
			img = s;
			animation_speed = as;
			state = 0;
			is_running = true;
			target = c;
			dmg = d;
		}

		bool is_animation_running() { return is_running; }

		void move()
		{
			if(actual_x == end_x)
			{
				if(state == 0)
				{
					end_x = start_x;
					state = 1;
					s->draw(actual_x, 0, img);
					animation_speed = -animation_speed;
					target->set_damage(dmg);
				}
				else
				{
					state = 0;
					is_running = false;
					s->draw(end_x, 0, img);
					BattleState::is_player_turn = !BattleState::is_player_turn;
				}
			}
			else
			{
				s->draw((actual_x+=animation_speed), 0, img);
			}
		}

	private:
		bool is_running;
		int animation_speed;
		int start_x;
		int state; // 0 - attack anim, 1 - back anim
		int actual_x;
		int end_x;
		SDL_Surface* img;
		Surface* s;
		Character* target;
		int dmg;
	};

	Animation* animation;
};
