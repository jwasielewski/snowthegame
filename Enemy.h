#pragma once
#include <string>
#include "SDL.h"
#include "Surface.h"
#include "Character.h"
#include "Debug.h"
class Enemy : public Character
{
public:
	Enemy(int,int,int,int);
	~Enemy(void);

	void draw();

	int ID();
	bool is_alive();
	int get_life_points();
	void set_damage(int);
	void set_id_xy(int,int,int);
	bool check_map_id(int);
	bool position(int,int);

#if DEBUG == 1
	std::string gp();
#endif

private:
	int life;
	int x, y;
	bool alive;
	int id;
	int map_id;
	Surface* s;
	SDL_Surface* surface;
	SDL_Rect frame;
};

