#pragma once
#include "Player.h"
#include "Surface.h"
#include "Textures.h"
#include "MapSurface.h"
#include "EnemyManager.h"
#include <vector>
#include <string>
class Map
{
public:
	Map(void);
	~Map(void);

	void render();
	void handle_events();
	std::string check_state();

private:
	Surface* main_window;
	MapSurface* maps[3];
	int current_map;
	Textures* t;
	Player* p;
	EnemyManager* em;
};
