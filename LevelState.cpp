#include "LevelState.h"
#include "GameStateManager.h"
#include "Surface.h"
#include "Event.h"
#include "Music.h"
#include "SDL_ttf.h"
#include <string>

LevelState::LevelState(GameStateManager::StateOptions options)
{
	_has_ended = false;
	s = Surface::getInstance();
	bg = s->load_optimized_image("assets/gra.png");
	font = TTF_OpenFont("assets/PressStart2P.ttf", 16);
	map = new Map;
	e = Event::getInstance();
	p = Player::getInstance();
	music = Music::getInstance();
	music->set_music(Music::BACKGROUND);
	c.r=255; c.g=0; c.b=0;
	player_health_txt = TTF_RenderText_Solid(font, "Punkty zycia ", c);
	clock = Clock::getInstance();
	clock->init();
}

LevelState::~LevelState(void)
{
	music->set_music(Music::NONE);
	SDL_FreeSurface(bg);
	SDL_FreeSurface(player_health);
	SDL_FreeSurface(player_health_txt);
}

void LevelState::handle_events()
{
	if(e->keys[SDLK_ESCAPE])
	{
		clock->pause();
		GameStateManager::StateOptions options;
		options[std::string("IN_GAME")] = std::string("TRUE");
		GameStateManager::getInstance()->request_next_state(
			GameStateManager::GameStates::STATE_MENU,
			options
		);
	}
	map->handle_events();
}

void LevelState::logic()
{
	std::string s = map->check_state();
	if(s == "WIN")
	{
		_has_ended = true;
		GameStateManager::StateOptions options;
		GameStateManager::getInstance()->request_next_state(
			GameStateManager::GameStates::STATE_WIN,
			options
		);
	}
	else if(s == "LOSE")
	{
		_has_ended = true;
		GameStateManager::StateOptions options;
		GameStateManager::getInstance()->request_next_state(
			GameStateManager::GameStates::STATE_LOSE,
			options
		);
	}
	music->set_music(Music::BACKGROUND);

	if(!clock->can_play())
	{
		_has_ended = true;
		GameStateManager::StateOptions options;
		GameStateManager::getInstance()->request_next_state(
			GameStateManager::GameStates::STATE_LOSE,
			options
		);
	}
	clock->tick();
}

void LevelState::render()
{
	map->render();

	SDL_Rect r = { 0, 608, 800, 37 };
	s->fill_rect(r, 0x000000);

	s->draw(10, 620, player_health_txt);

	SDL_FreeSurface(player_health);
	player_health = TTF_RenderText_Solid(font, std::to_string(p->get_life_points()).c_str(), c);
	s->draw(player_health_txt->w+5, 620, player_health);

	clock->draw();
}

bool LevelState::has_ended()
{
	return _has_ended;
}