#pragma once
#include "SDL.h"
#include <string>
class Surface
{
public:
	~Surface(void);
	bool init();
	void refresh();
	void draw(int, int, SDL_Surface*, SDL_Rect* e=NULL);
	void fill_rect(SDL_Rect, int);
	SDL_Surface* load_optimized_image(char*);
	SDL_Event* get_window_event();
	static Surface* getInstance();
private:
	Surface(void);
	SDL_Surface* main_window;

	const int WINDOW_WIDTH;
	const int WINDOW_HEIGHT;
	const int WINDOW_BPP;
	const std::string WINDOW_TITLE;
};

