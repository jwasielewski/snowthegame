#include "Event.h"
#include "SDL.h"

Event::Event(void)
{
	for(auto &x : keys)
	{
		x = false;
	}
}

Event::~Event(void)
{
}

Event* Event::getInstance()
{
	static Event e;
	return &e;
}

void Event::handle_events()
{
	if(SDL_PollEvent(&e))
	{
		if(e.type == SDL_KEYDOWN)
		{
			keys[e.key.keysym.sym] = true;
		}
		else if(e.type == SDL_KEYUP)
		{
			keys[e.key.keysym.sym] = false;
		}
	}
}