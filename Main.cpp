#include "Game.h"
#include <iostream>

int main(int argc, char** argv)
{
	Game *game = new Game;
	if(!game->init())
	{
		std::cerr << "[ERROR] Blad podczas ladowania gry\n";
	}
	game->run();
	delete game;
	return 0;
}