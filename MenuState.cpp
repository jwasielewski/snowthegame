#include "MenuState.h"
#include "GameStateManager.h"
#include "Surface.h"
#include "Event.h"
#include "Clock.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"
#include <map>
#include <string>

MenuState::MenuState(GameStateManager::StateOptions options)
{
	_has_ended = false;
	if(options["IN_GAME"] == "TRUE")
	{
		_in_game = true;
	}
	else
	{
		_in_game = false;
	}
	Mix_PauseMusic();
	title = Surface::getInstance()->load_optimized_image("assets/game_menu.png");
}

MenuState::~MenuState(void)
{
	Mix_ResumeMusic();
	SDL_FreeSurface(title);
}

bool MenuState::has_ended()
{
	return _has_ended;
}

void MenuState::handle_events()
{
	if(Event::getInstance()->keys[SDLK_RETURN])
	{
		_has_ended = true;
		if(!_in_game)
		{
			GameStateManager::getInstance()->request_next_state(
				GameStateManager::GameStates::STATE_LEVEL,
				GameStateManager::StateOptions()
			);
		}
		else
		{
			Clock::getInstance()->start();
		}
	}
}

void MenuState::logic()
{

}

void MenuState::render()
{
	Surface::getInstance()->draw(0, 0, title);
}