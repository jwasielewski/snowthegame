#pragma once
#include "gamestate.h"
#include "Event.h"
#include "Surface.h"
#include "SDL.h"
#include "GameStateManager.h"
class WinState : public GameState
{
public:
	WinState(GameStateManager::StateOptions options);
	~WinState(void);

	void handle_events();
	void logic();
	void render();
	bool has_ended();

private:
	Event* e;
	Surface* s;
	SDL_Surface* bg;
	bool _has_ended;
};

