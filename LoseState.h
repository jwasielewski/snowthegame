#pragma once
#include "GameState.h"
#include "GameStateManager.h"
#include "Event.h"
#include "Surface.h"
#include "SDL.h"
class LoseState : public GameState
{
public:
	LoseState(GameStateManager::StateOptions);
	~LoseState(void);

	void handle_events();
	void logic();
	void render();
	bool has_ended();

private:
	bool _has_ended;
	Event* e;
	Surface* s;
	SDL_Surface* bg;
};

