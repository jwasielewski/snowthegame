#pragma once
#include "GameState.h"
#include "GameStateManager.h"
#include "Music.h"
class MenuState : public GameState
{
public:
	MenuState(GameStateManager::StateOptions options);
	~MenuState(void);

	void handle_events();
	void logic();
	void render();
	void set_main_window(SDL_Surface* mw);
	bool has_ended();

private:
	bool _has_ended;
	bool _in_game;
	SDL_Surface* title;
	Music* m;
};

