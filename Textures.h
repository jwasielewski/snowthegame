#pragma once
#include "SDL.h"
#include <vector>
class Textures
{
public:
	Textures(char *);
	~Textures(void);

	typedef struct __Tile
	{
		char c;
		bool can_go;
		SDL_Rect rect;

		static __Tile mt(char h, SDL_Rect r, bool cg)
		{
			__Tile t;
			t.c = h;
			t.rect = r;
			t.can_go = cg;
			return t;
		}

	} Tile;

	SDL_Surface* Tiles;
	
	SDL_Rect r(char);
	bool cg(char);

private:
	void load_config(char *path);
	std::vector<Tile> tiles_container;
};
