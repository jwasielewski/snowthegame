#pragma once
#include "SDL.h"
#include "Character.h"
class Player : public Character
{
public:
	static Player* getInstance();
	~Player(void);
	void render();
	void handle_events();

	void set_position(int,int);
	int gx() { return this->x; }
	int gy() { return this->y-1; }

	int get_life_points() { return life; }
	void set_damage(int p) { life -= p; }

	void up();
	void down();
	void left();
	void right();
	
	enum Direction {DOWN = 0, LEFT, RIGHT, UP};
	void set_direction(Player::Direction d);
	Player::Direction get_direction() { return direction; }

	bool is_animation_running();

private:
	Player(void);

	int life;
	int x,y;
	SDL_Rect rect;
	SDL_Surface* surface;

	Direction direction;
	SDL_Rect frames[4];
	struct _animation
	{
		int frame;
		int x, y;
		int end_x, end_y;
		bool is_running;
	} animation;
	const int ANIMATION_SPEED;
	inline SDL_Rect get_anim_frame();
};

