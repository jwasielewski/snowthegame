#include "Surface.h"
#include "SDL.h"
#include "SDL_image.h"
#include <string>

Surface::Surface(void) : WINDOW_WIDTH(800), WINDOW_HEIGHT(645), WINDOW_BPP(32), WINDOW_TITLE("Snow The Game")
{
}

Surface::~Surface(void)
{
	SDL_FreeSurface(main_window);
}

Surface* Surface::getInstance()
{
	static Surface* instance = new Surface;
	return instance;
}

bool Surface::init()
{
	SDL_ShowCursor(0);
	SDL_putenv("SDL_VIDEO_CENTERED=center");
	main_window = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_BPP, SDL_SWSURFACE);
	if(main_window == NULL)
	{
		return false;
	}

	SDL_WM_SetCaption(WINDOW_TITLE.c_str(), NULL);

	return true;
}

void Surface::draw(int x, int y, SDL_Surface* source, SDL_Rect* rect)
{
	SDL_Rect title_coords;
	title_coords.x = x;
	title_coords.y = y;
	SDL_BlitSurface(source, rect, main_window, &title_coords);
}

void Surface::fill_rect(SDL_Rect r, int c)
{
	SDL_FillRect(main_window, &r, c);
}

SDL_Surface* Surface::load_optimized_image(char *path)
{
	SDL_Surface *img = NULL, *tmp;

	tmp = IMG_Load(path);

	if(tmp != NULL)
	{
		img = SDL_DisplayFormat(tmp);
		SDL_FreeSurface(tmp);
	}

	return img;
}

void Surface::refresh()
{
	SDL_Flip(main_window);
}