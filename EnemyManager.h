#pragma once
#include "Enemy.h"
#include <vector>
class EnemyManager
{
public:
	static EnemyManager* getInstance();
	~EnemyManager(void);

	void configure();

	std::vector<int> get_enemies_id(int);

	Enemy* at(int);
	int get_life_at();

private:
	EnemyManager(void);
	std::vector<Enemy*> v;
	int i;
};

