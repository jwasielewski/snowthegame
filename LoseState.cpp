#include "LoseState.h"
#include "SDL_mixer.h"

LoseState::LoseState(GameStateManager::StateOptions options)
{
	e = Event::getInstance();
	s = Surface::getInstance();
	_has_ended = false;
	bg = s->load_optimized_image("assets/lose.png");
	Mix_VolumeMusic(0);
	Mix_HaltMusic();
}


LoseState::~LoseState(void)
{
	SDL_FreeSurface(bg);
}

void LoseState::handle_events()
{
	if (e->keys[SDLK_ESCAPE])
	{
		_has_ended = true;
	}
}

void LoseState::logic()
{
	return;
}

void LoseState::render()
{
	s->draw(0, 0, bg);
}

bool LoseState::has_ended()
{
	return _has_ended;
}
