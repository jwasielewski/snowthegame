#pragma once
#include "SDL.h"
class GameState
{
public:
	virtual ~GameState(void);
	virtual void handle_events() = 0;
	virtual void logic() = 0;
	virtual void render() = 0;
	virtual bool has_ended() = 0;
};

