#include "GameStateManager.h"
#include "GameState.h"
#include "MenuState.h"
#include "LevelState.h"
#include "WinState.h"
#include "BattleState.h"
#include "LoseState.h"

GameStateManager::GameStateManager(void)
{
	states = new GameState*[32];
	states_counter = -1;
}

GameStateManager::~GameStateManager(void)
{
}

GameStateManager* GameStateManager::getInstance()
{
	static GameStateManager* gsm = new GameStateManager;
	return gsm;
}

void GameStateManager::request_next_state(GameStateManager::GameStates state, StateOptions options)
{
	check_last_state();

	switch (state)
	{
	case GameStateManager::STATE_MENU:
		states[states_counter] = new MenuState(options);
		break;
	case GameStateManager::STATE_LEVEL:
		states[states_counter] = new LevelState(options);
		break;
	case GameStateManager::STATE_BATTLE:
		states[states_counter] = new BattleState(options);
		break;
	case GameStateManager::STATE_WIN:
		states[states_counter] = new WinState(options);
		break;
	case GameStateManager::STATE_LOSE:
		states[states_counter] = new LoseState(options);
		break;
	default:
		break;
	}

	++states_counter;
}

void GameStateManager::check_last_state()
{
	if(states_counter != -1 && states[states_counter-1]->has_ended())
	{
		delete states[states_counter-1];
		--states_counter;
	}
}

GameState* GameStateManager::get_actual_state()
{
	if(states_counter == -1)
	{
		return NULL;
	}
	return states[states_counter-1];
}
