#pragma once
#include "SDL_mixer.h"
class Music
{
public:
	~Music(void);

	static Music* getInstance();
	bool init();

	enum MUSIC { BACKGROUND, BATTLE, NONE };
	void play();
	void set_music(MUSIC);

private:
	Music(void);

	Mix_Music *background;
	Mix_Music *battle;
	Mix_Music *active_music;
};

