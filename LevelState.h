#pragma once
#include "GameState.h"
#include "GameStateManager.h"
#include "SDL.h"
#include "Map.h"
#include "Event.h"
#include "Music.h"
#include "Surface.h"
#include "Player.h"
#include "Clock.h"
#include "SDL_ttf.h"
class LevelState : public GameState
{
public:
	LevelState(GameStateManager::StateOptions options);
	~LevelState(void);

	void handle_events();
	void logic();
	void render();
	bool has_ended();
private:
	bool _has_ended;
	SDL_Surface* bg;
	Map* map;
	Event* e;
	Music* music;
	Surface* s;
	Player* p;
	SDL_Surface* player_health;
	SDL_Surface* player_health_txt;
	TTF_Font* font;
	SDL_Color c;
	Clock* clock;
};

