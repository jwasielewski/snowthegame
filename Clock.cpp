#include "Clock.h"
#include "Debug.h"
#include <sstream>

#if DEBUG == 1
#include <iostream>
#endif

Clock::Clock(void) : MAX_TIME(600)
{
	font = TTF_OpenFont("assets/PressStart2P.ttf", 16);
	c.r=255; c.g=0; c.b=0;
	s = Surface::getInstance();
	ramaining_time = TTF_RenderText_Solid(font, "Pozostaly czas", c);
	can_tick = true;
}

Clock::~Clock(void)
{
	SDL_FreeSurface(time_txt);
	SDL_FreeSurface(ramaining_time);
	//zwalnianie pami�ci powoduje rzuceniem wyj�tku w msvc_co�tamco�tam (libfreetype-6.dll)
	//TTF_CloseFont(font);
}

Clock* Clock::getInstance()
{
	static Clock c;
	return &c;
}

void Clock::init()
{
	time_start = last_time = std::time(0);
}

void Clock::start()
{
	can_tick = true;
	resume_time = std::time(0);
#if DEBUG == 1
std::cout << "time_start " << static_cast<int>(time_start) << std::endl;
std::cout << "pause_time " << static_cast<int>(pause_time) << std::endl;
std::cout << "pause_time " << static_cast<int>(resume_time) << std::endl;
std::cout << "difference " << static_cast<int>(std::difftime(resume_time, pause_time)) << std::endl;
#endif
	time_start = time_start + static_cast<int>(std::difftime(resume_time, pause_time));
	pause_time = 0;
}

void Clock::pause()
{
	can_tick = false;
	pause_time = std::time(0);
}

void Clock::tick()
{
	if(can_tick && std::difftime(std::time(0), last_time) >= 1)
	{
		last_time = time(0);
		difference = static_cast<int>(std::difftime(last_time, time_start));
	}
}

bool Clock::can_play()
{
	if(MAX_TIME - difference > 0) return true;
	return false;
}

void Clock::draw()
{
	SDL_FreeSurface(time_txt);
	time_txt = TTF_RenderText_Solid(font, std::to_string(MAX_TIME - difference).c_str(), c);
	s->draw(800 - ramaining_time->w - time_txt->w - 20, 620, ramaining_time);
	s->draw(800 - time_txt->w - 10, 620, time_txt);
}
