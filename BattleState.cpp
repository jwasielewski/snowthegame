#include "BattleState.h"
#include "GameStateManager.h"
#include <string>

bool BattleState::is_player_turn = true;

BattleState::BattleState(GameStateManager::StateOptions options)
	: PLAYER_ATTACK_STRONG(20), PLAYER_ATTACK_WEAK(10), ENEMY_ATTACK(5), PLAYER_FATALITY_ATTACK(100)
{
	p = Player::getInstance();
	em = EnemyManager::getInstance();
	s = Surface::getInstance();
	e = Event::getInstance();
	clock = Clock::getInstance();
	m = Music::getInstance();
	m->set_music(Music::BATTLE);

	enemy_id = atoi(options[std::string("ENEMY_ID")].c_str());

	_has_ended = false;

	player_image = s->load_optimized_image("assets/player_avatar.png");
	enemy_image = s->load_optimized_image("assets/enemy_avatar.png");
	bg = s->load_optimized_image("assets/battle_bg.png");

	c.r=255; c.g=0; c.b=0;
	pturn.r=0; pturn.g=255; pturn.b=0;
	eturn.r=0; eturn.g=255; eturn.b=255;

	font = TTF_OpenFont("assets/PressStart2P.ttf", 16);
	player_turn = TTF_RenderText_Solid(font, "Twoja tura", pturn);
	enemy_turn = TTF_RenderText_Solid(font, "Tura przeciwnika", eturn);
	strong_attack = TTF_RenderText_Solid(font, "H - mocny atak (20 pkt)", c);
	weak_attack = TTF_RenderText_Solid(font, "J - slaby atak (10 pkt)", c);

	is_player_turn = true;

	animation = new Animation(s);
}


BattleState::~BattleState(void)
{
	m->set_music(Music::NONE);
	SDL_FreeSurface(player_image);
	SDL_FreeSurface(enemy_image);
	SDL_FreeSurface(player_health);
	SDL_FreeSurface(enemy_health);
	SDL_FreeSurface(strong_attack);
	SDL_FreeSurface(weak_attack);
	SDL_FreeSurface(player_turn);
	SDL_FreeSurface(enemy_turn);
	SDL_FreeSurface(bg);
	TTF_CloseFont(font);
	delete animation;
}

void BattleState::handle_events()
{
	if(animation->is_animation_running()) return;
	if(is_player_turn)
	{
		if(e->keys[SDLK_h])
		{
			animation->set_params(0, 220, 10, player_image, em->at(enemy_id), PLAYER_ATTACK_STRONG);
		}
		else if(e->keys[SDLK_j])
		{
			animation->set_params(0, 220, 10, player_image, em->at(enemy_id), PLAYER_ATTACK_WEAK);
		}
		else if(e->keys[SDLK_f])
		{
			animation->set_params(0, 220, 10, player_image, em->at(enemy_id), PLAYER_FATALITY_ATTACK);
		}
	}
}

void BattleState::logic()
{
	if(em->at(enemy_id)->get_life_points() == 0 && !animation->is_animation_running())
	{
		_has_ended = true;
	}
	else if(!is_player_turn && !animation->is_animation_running())
	{
		animation->set_params(800-enemy_image->w, 800-enemy_image->w-220, -10, enemy_image, p, ENEMY_ATTACK);
	}
	if(p->get_life_points() <= 0)
	{
		_has_ended = true;
	}

	clock->tick();
}

void BattleState::render()
{
	s->draw(0,0,bg);

	clock->draw();

	SDL_Rect r1 = {46, 410, p->get_life_points()*2, 5};
	SDL_Rect r2 = {563, 410, em->at(enemy_id)->get_life_points()*2, 5};
	s->fill_rect(r1, 0xFF0000);
	s->fill_rect(r2, 0xFF0000);

	SDL_FreeSurface(player_health);
	player_health = TTF_RenderText_Solid(font, std::to_string(p->get_life_points()).c_str(), c);
	s->draw((player_image->w - player_health->w)/2, 420, player_health);

	SDL_FreeSurface(enemy_health);
	enemy_health = TTF_RenderText_Solid(font, std::to_string(em->at(enemy_id)->get_life_points()).c_str(), c);
	s->draw(770-((enemy_image->w - enemy_health->w)/2), 420, enemy_health);

	s->draw(10, 500, strong_attack);
	s->draw(10, 550, weak_attack);

	(is_player_turn) ? s->draw(500, 550, player_turn) : s->draw(500, 550, enemy_turn);

	if(is_player_turn && !animation->is_animation_running())
	{
		s->draw(800-enemy_image->w, 0, enemy_image);
		s->draw(0, 0, player_image);
	}
	else if(!is_player_turn && animation->is_animation_running())
	{
		s->draw(0, 0, player_image);
	}
	else if(is_player_turn && animation->is_animation_running())
	{
		s->draw(800-enemy_image->w, 0, enemy_image);
	}

	if(animation->is_animation_running())
	{
		animation->move();
	}
}

bool BattleState::has_ended()
{
	return _has_ended;
}
