#include "Map.h"
#include "Surface.h"
#include "SDL.h"
#include <iostream>

Map::Map(void)
{
	main_window = Surface::getInstance();
	t = new Textures("config/tiles.stgc");
	em = EnemyManager::getInstance();
	em->configure();
	p = Player::getInstance();
	current_map = 0;
	maps[0] = new MapSurface("maps/map00.m", t, p, em, 0);
	maps[0]->configure();
	maps[1] = new MapSurface("maps/map01.m", t, p, em, 1);
	maps[2] = new MapSurface("maps/map02.m", t, p, em, 2);
}

Map::~Map(void)
{
	delete maps[0];
	delete maps[1];
	delete maps[2];
	delete t;
	delete em;
	delete p;
}

void Map::render()
{
	maps[current_map]->render();
}

void Map::handle_events()
{
	maps[current_map]->handle_events();
}

std::string Map::check_state()
{
	std::string state = maps[current_map]->check_state();
	if(state == "" || state == "WIN" || state == "LOSE")
	{
		return state;
	} 
	else if (state == "NEXT_LEVEL")
	{
		++current_map;
		maps[current_map]->configure();
	}
	return "";
}
