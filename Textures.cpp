#include "Textures.h"
#include "SDL.h"
#include "Surface.h"
#include <vector>
#include <fstream>

Textures::Textures(char *path)
{
	load_config(path);
}

Textures::~Textures(void)
{
	SDL_FreeSurface(Tiles);
}

void Textures::load_config(char *path)
{
	std::ifstream file;
	file.open(path);
	if(!file.is_open())
	{
		file.close();
		return;
	}

	Tiles = Surface::getInstance()->load_optimized_image("assets/grounds.png");

	SDL_Rect rect;
	char c; int cw;
	while((file >> c >> rect.x >> rect.y >> rect.w >> rect.h >> cw))
	{
		tiles_container.push_back(Tile::mt(c, rect, (cw == 0) ? false : true));
	}

	file.close();
}

SDL_Rect Textures::r(char c)
{
	for(auto a : tiles_container)
	{
		if(a.c == c) return a.rect;
	}
	SDL_Rect r = {0,0,0,0};
	return r;
}

bool Textures::cg(char c)
{
	for(auto a : tiles_container)
	{
		if(a.c == c) return a.can_go;
	}
	return true;
}