#pragma once
#include "SDL.h"
#include "SDL_ttf.h"
#include "Surface.h"
#include <ctime>
class Clock
{
public:
	~Clock(void);

	static Clock* getInstance();
	void init();
	void start();
	void pause();
	void tick();
	bool can_play();
	void draw();

private:
	Clock(void);

	SDL_Surface* time_txt;
	SDL_Surface* ramaining_time;
	TTF_Font* font;
	SDL_Color c;
	Surface* s;

	std::time_t time_start, last_time, pause_time, resume_time;
	int time_left;
	int difference;

	bool can_tick;

	const int MAX_TIME;
};

